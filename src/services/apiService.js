import axios from "axios";
import * as SecureStore from "expo-secure-store";
import { Alert } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';


//Configuração global da API no AXIOS
const api = axios.create({
  baseURL: "https://wolfbook.herokuapp.com/",
  headers: { "Content-Type": "application/json" },
});

//Função para setar o token
const setToken = (token) => {
  return SecureStore.setItemAsync("userToken", token);
};

//Função para recuperar o token
const getToken = () => {
  return SecureStore.getItemAsync("userToken");
  //.catch(error => console.log("Could not save user info ", error));
};

//Função para apagar o token
const deleteToken = () => {
    return SecureStore.deleteItemAsync("userToken");
    //.catch(error => console.log("Could not delete user info ", error));
};


// Funções de chamada da API
// Endpoint Login
async function login(email, password) {
  let userData = null;
  await api
  .post("/login", {      
    user: {
      email: email,
      password: password,
    },
  })
  .then((response) => {
    userData = response.data;     



  })
  .catch((error) => {
    console.log(error)      
    Alert.alert(
      "Dados inválidos! Verifique as informações e tente novamente!"
    );
  });
  return userData;
}


export {
  login,
};
