import React from "react";

export const UserContext = React.createContext();

export const UserStorage = ({ children }) => {
  const [userContextData, setUserContextData] = React.useState("");

  return (
    <UserContext.Provider value={{ userContextData, setUserContextData }}>
      {children}
    </UserContext.Provider>
  );
};
