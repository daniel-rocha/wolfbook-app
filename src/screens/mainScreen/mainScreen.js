import React, { useState, useEffect } from "react";
import {SafeAreaView, StyleSheet, View, TextInput, Text, TouchableOpacity, Image, Alert, Button} from 'react-native';
import { UserContext } from "../../components/userContext";
import { Avatar } from 'react-native-paper';

export default MainScreen = (props) => {
  // States
  const [tempo, setTempo] = useState('')
  const [assunto, setAssunto] = useState('')  
  const { userContextData, setUserContextData } = React.useContext(UserContext);

  const handleNotification = async (tempo) => {
    //Inserir função para cadastrar notificações
    //https://docs.expo.io/versions/latest/sdk/notifications/
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View>
        <View style={{ marginLeft: '3%', marginVertical: '5%', flexDirection: 'row' }}>
          <Avatar.Text size={64} label={userContextData.name.charAt(0)} />
          <View style={styles.viewAvatarText}>
          <Text style={styles.avatarText}>{userContextData.name} </Text>
          </View>
        </View>
        

        <Text style={styles.titleText}>Notifique-me que</Text>

        <TextInput
          placeholder="assunto"
          value={assunto}
          onChangeText={(data) => setAssunto(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <Text style={styles.titleText}> em </Text>

        <TextInput
          placeholder="minutos"
          value={tempo}
          onChangeText={(data) => setTempo(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TouchableOpacity
          onPress={() => handleNotification(tempo)}
          style={styles.buttonStyle}
        >
          <Text style={styles.buttonTextStyle}>Cadastrar Notificação</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.buttonStyle}
        >
          <Text style={styles.buttonTextStyle}>Voltar</Text>
        </TouchableOpacity>

        <Text></Text>


      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "white",
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    paddingVertical: 20,
  },
  buttonTextStyle: {
    padding: 5,
    color: "white",
    textAlign: "center",
  },
  buttonStyle: {
    fontSize: 16,
    color: "white",
    backgroundColor: "green",
    padding: 5,
    marginTop: 32,
    minWidth: 250,
    width: '80%',
    alignSelf:"center",
  },
  viewAvatarText: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarText: {
    marginLeft: '2%',
    fontSize: 24,
    fontWeight: "bold",    
  },
  textInputStyle: {
    marginVertical: "4%",
    textAlign: "center",
    height: 40,
    width: "100%",
    borderWidth: 1,
    borderColor: "green",
  },
});
