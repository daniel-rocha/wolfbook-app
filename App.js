import React, { useState } from 'react';
import {SafeAreaView, StyleSheet, View, TextInput, Text, TouchableOpacity, Image, Alert, Button} from 'react-native';
//import React Navigation - StackNavigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//import Axios
import axios from "axios";
//import Screens
import InitialScreen from './src/screens/initialScreen/initialScreen'
import MainScreen from './src/screens/mainScreen/mainScreen'
import SignUpScreen from './src/screens/signupScreen/signupScreen'

//import do Context do usuário (alterativa a asyncStorage)
import { UserStorage } from "./src/components/userContext";

//Configuração global da API no AXIOS
const api = axios.create({
  baseURL: "https://wolfbook.herokuapp.com/",
  headers: { "Content-Type": "application/json" },
});

const Stack = createStackNavigator();

export default function App() {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <UserStorage>
      <NavigationContainer >
          <Stack.Navigator initialRouteName="Initial">          
            <Stack.Screen name="Initial" component={InitialScreen} />
            <Stack.Screen name="Signup" component={SignUpScreen} />
            <Stack.Screen name="Main" component={MainScreen} />
          </Stack.Navigator>
      </NavigationContainer>
    </UserStorage>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white',
  },
  titleText: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
  },
  // textStyle: {
  //   marginVertical: '2%',
  //   padding: 10,
  //   textAlign: 'center',
  // },
  buttonStyle: {
    fontSize: 16,
    color: 'white',
    backgroundColor: 'green',
    padding: 5,
    marginTop: 32,
    minWidth: 250,
  },
  buttonTextStyle: {
    padding: 5,
    color: 'white',
    textAlign: 'center',
  },
  textInputStyle: {
    marginVertical: '4%',
    textAlign: 'center',
    height: 40,
    width: '100%',
    borderWidth: 1,
    borderColor: 'green',
  },
  
  viewPickers: {
    //marginHorizontal: '2%',
    flexDirection: 'row',
    //width: '80%',
    alignSelf: 'center',
  },
  dddPickers: {
    marginHorizontal: '5%',
    //backgroundColor: 'green',
  },
  form: {
    marginTop: '5%'
  },
  itemSelect: {
    color: 'red',
  },
  a:{

   },
  pickerStyle: {
    width: 100,
    fontSize: 16,
    //backgroundColor: 'red',
    marginBottom: '8%',
    
    //marginLeft: '3%',
  },
  // imgView: {
  //   //backgroundColor: 'green',
  // },
  tinyLogo: {
    //marginTop: '10%',
  },
});
