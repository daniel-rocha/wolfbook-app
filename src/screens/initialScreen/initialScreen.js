import React, { useState, useEffect } from "react";
import {SafeAreaView, StyleSheet, View, TextInput, Text, TouchableOpacity, Image, Alert, Button} from 'react-native';
import { Divider } from 'react-native-elements';
// Importando o Axios
import * as api from '../../services/apiService'
// Importando o SecureStore
import * as SecureStore from "expo-secure-store";
// Importando o Context
import { UserContext } from "../../components/userContext";


//Função para setar o token no SecureStore
const setToken = (token) => {
  return SecureStore.setItemAsync("userToken", token);
};


export default InitialScreen = (props) => {
  // States
  const [loginText, setLoginText] = useState("");
  const [pwdText, setPwdText] = useState("");
  // Context
  const { userContextData, setUserContextData } = React.useContext(UserContext);

  const doLogin = async (loginText, pwdText) => {
    const res = await api.login(loginText, pwdText) 
    setToken(res.token);
    setUserContextData(res.user);
    res.token ? props.navigation.navigate("Main") : null 
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.titleText}>Wolfbook - Cadastro de Lembretes</Text>

        <Divider style={{ backgroundColor: "blue" }} />

        <TextInput
          placeholder="Login"
          value={loginText}
          onChangeText={(data) => setLoginText(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TextInput
          placeholder="Senha"
          value={pwdText}
          onChangeText={(data) => setPwdText(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
          secureTextEntry={true}
        />

        <Divider style={{ backgroundColor: "blue" }} />

        <TouchableOpacity
          onPress={() => doLogin(loginText, pwdText)}
          style={styles.buttonStyle}
        >
          <Text style={styles.buttonTextStyle}>Logar</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("Signup")}
          style={styles.buttonStyle}
        >
          <Text style={styles.buttonTextStyle}>Cadastrar</Text>
        </TouchableOpacity>

        <Divider style={{ backgroundColor: "blue" }} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "white",
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    paddingVertical: 20,
  },
  buttonStyle: {
    fontSize: 16,
    color: "white",
    backgroundColor: "green",
    padding: 5,
    marginTop: 32,
    minWidth: 250,
  },
  buttonTextStyle: {
    padding: 5,
    color: "white",
    textAlign: "center",
  },
  textInputStyle: {
    marginVertical: "4%",
    textAlign: "center",
    height: 40,
    width: "100%",
    borderWidth: 1,
    borderColor: "green",
  },
  viewPickers: {
    flexDirection: "row",
    alignSelf: "center",
  },
  form: {
    marginTop: "5%",
  },
  itemSelect: {
    color: "red",
  },
  pickerStyle: {
    width: 100,
    fontSize: 16,
    marginBottom: "8%",
  },
});
