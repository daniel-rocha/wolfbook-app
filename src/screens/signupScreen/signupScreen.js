import React, { useState, useEffect } from "react";
import {SafeAreaView, StyleSheet, View, TextInput, Text, TouchableOpacity, Image, Alert, Button} from 'react-native';
import { Divider } from 'react-native-elements';
import { CheckBox } from 'react-native-elements'
import DateTimePickerModal from "react-native-modal-datetime-picker";

export default SignUpScreen = (props) => {
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [pwd, setPwd] = useState("");
  const [pwdConf, setPwdConf] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn("A date has been picked: ", date);
    hideDatePicker();
    setBirthdate(date);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.form}>        
        <TextInput
          placeholder="Nome"
          value={nome}
          onChangeText={(data) => setNome(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TextInput
          placeholder="Email"
          textContentType="emailAddress"
          value={email}
          onChangeText={(data) => setEmail(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TextInput
          placeholder="Gênero"
          value={gender}
          onChangeText={(data) => setGender(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TextInput
          placeholder="Senha"
          value={pwd}
          onChangeText={(data) => setPwd(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <TextInput
          placeholder="Confirme sua senha"
          value={pwdConf}
          onChangeText={(data) => setPwdConf(data)}
          underlineColorAndroid="transparent"
          style={styles.textInputStyle}
        />

        <Button title="Data de Nascimento" onPress={showDatePicker} />
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          mode="date"
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "white",
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    paddingVertical: 20,
  },
  // textStyle: {
  //   marginVertical: '2%',
  //   padding: 10,
  //   textAlign: 'center',
  // },
  buttonStyle: {
    fontSize: 16,
    color: "white",
    backgroundColor: "green",
    padding: 5,
    marginTop: 32,
    minWidth: 250,
  },
  buttonTextStyle: {
    padding: 5,
    color: "white",
    textAlign: "center",
  },
  textInputStyle: {
    marginVertical: "4%",
    textAlign: "center",
    height: 40,
    width: "100%",
    borderWidth: 1,
    borderColor: "green",
  },

  viewPickers: {
    //marginHorizontal: '2%',
    flexDirection: "row",
    //width: '80%',
    alignSelf: "center",
  },
  dddPickers: {
    marginHorizontal: "5%",
    //backgroundColor: 'green',
  },
  form: {
    marginTop: "5%",
  },
  itemSelect: {
    color: "red",
  },
  a: {},
  pickerStyle: {
    width: 100,
    fontSize: 16,
    //backgroundColor: 'red',
    marginBottom: "8%",

    //marginLeft: '3%',
  },
  // imgView: {
  //   //backgroundColor: 'green',
  // },
  tinyLogo: {
    //marginTop: '10%',
  },
});
